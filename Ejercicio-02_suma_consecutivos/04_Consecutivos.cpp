#include<iostream>

using namespace std;

main(){
	int suma=0,numero=0,i;
	do{
		cout<<"Ingresa un numero del 1 al 50:\n";
		cin>>numero;
		if(numero<1 || numero>50)
			cout<<"El numero no esta dentro del parametro solicitado. Intentar nuevamente\n";
	}while(numero<1 || numero>50);
	
	for(i=0;i<=numero;i++){
		suma = suma + i;
	}
	cout<<"El resultado de la suma de consecutivos hasta el numero "<<numero<<" es: "<<suma<<endl;	
}
