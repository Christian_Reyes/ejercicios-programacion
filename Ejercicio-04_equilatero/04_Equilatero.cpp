#include<iostream>
using namespace std;

main(){
	float lado, perimetro;
	do{
		cout<<"Indique el lado de un triangulo equilatero:\n";
		cin>>lado;
		if(lado<=0)
			cout<<"El lado de un triangulo debe tener un valor positivo mayor a 0\n";
	}while(lado<=0);
	perimetro = lado * 3;
	cout<<"El perimetro del triangulo es de: "<<perimetro;
}
