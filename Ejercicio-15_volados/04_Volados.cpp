#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;

main(){
	int bolsaCom = 500, bolsaUs = 500, volado, eleccion, ronda = 1, apuestaCom, apuestaUs, conteo = 0;
	srand(time(NULL));
	
	cout<<"Juego de Volados\n\n";
	cout<<"Bolsa inicial de $500 para la maquina y para el usuario\n\n";
	while(ronda<4){
		cout<<"Ronda "<<ronda<<"\n\n";
		apuestaCom = rand() % 80 + 20;
		cout<<"La computadora apuesta $"<<apuestaCom<<endl;
		do{
			cout<<"Cual sera el monto de su apuesta? (De $20 a $100) \n$";
			cin>>apuestaUs;
			if(apuestaUs<20 || apuestaUs>100)
				cout<<"El monto de apuesta esta fuera del limite indicado. Vuelva a intentar\n\n";
		}while(apuestaUs<20 || apuestaUs>100);
		cout<<"Elija el lado de la moneda\n\n";
		cout<<"1. Aguila\n";
		cout<<"2. Sol\n";
		cin>>eleccion;
		cout<<"Realizando el volado...\n\n";
		system("PAUSE()");
		system("cls");
		volado = rand() % 2 + 1;
		if(volado == 1)
			cout<<"\nEl resultado es Aguila\n\n";
		else
			cout<<"\nEl resultado es Sol\n\n";
		
		system("PAUSE()");	
		system("cls");
		if(eleccion == volado){
			cout<<"Ganaste esta ronda. Has ganado $"<<apuestaCom<<endl;
			bolsaUs = bolsaUs + apuestaCom;
			bolsaCom = bolsaCom - apuestaCom;
			conteo++;
		}else{
			cout<<"Perdiste esta ronda. Has perdido $"<<apuestaUs<<endl;
			bolsaUs = bolsaUs - apuestaUs;
			bolsaCom = bolsaCom + apuestaUs;
		}
		if(ronda != 3){
			cout<<"\nBolsa actual de la computadora: $"<<bolsaCom<<endl;
			cout<<"\nTu bolsa actual: $"<<bolsaUs<<endl;
		}
		ronda++;
		system("PAUSE()");	
		system("cls");
	}
	cout<<"\n\nResultado final\n\n";
	cout<<"Bolsa de la computadora final: $"<<bolsaCom<<endl;
	cout<<"Tu bolsa final: $"<<bolsaUs<<endl;
	cout<<"Rondas que ganaste: "<<conteo<<endl;
	cout<<"Rondas que gano la computadora: "<<3 - conteo<<endl;
	if(conteo >= 2)
		cout<<"\n\nHas ganado el juego!!!";
	else
		cout<<"\n\nHas perdido el juego. Suerte para la proxima vez";
}
