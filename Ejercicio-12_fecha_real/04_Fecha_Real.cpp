#include<iostream>
using namespace std;

int bisiesto(int anio){
	if(anio % 4 == 0)
		if (anio % 100 == 0)
			if (anio % 400 == 0)
				return 29;
			else
				return 28;
		else
			return 29;
	else
		return 28;
}

main(){
	int dia, mes, anio,real;
	
	cout<<"Indica un dia -DD-\n";
	cin>>dia;
	cout<<"Indica un mes -MM-\n";
	cin>>mes;
	cout<<"Indica un a�o -AAAA-\n";
	cin>>anio;
	if(mes >= 1 && mes <= 12){
		if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12){
			if(dia>=1 && dia<=31)
				real = 1;
			else
				real = 0;
		}else if(mes == 2){
			if(dia>=1 && dia<=bisiesto(anio))
				real = 1;
			else
				real = 0;
		}else{
			if(dia>=1 && dia<=30)
				real = 1;
			else
				real = 0;
		}
	}
	cout<<dia<<"/"<<mes<<"/"<<anio;
	if(real == 1)
		cout<<"\nLa fecha es real";
	else
		cout<<"\nLa fecha es irreal";
}
