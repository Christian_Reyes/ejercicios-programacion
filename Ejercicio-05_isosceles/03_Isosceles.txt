Inicio
	Numerico ladoIgual = 0
	Numerico ladoDiferente = 0
	Numerico perimetro = 0
	do
		Leer "Indique el tamano de uno de los lados iguales de un triangulo isosceles", ladoIgual
		Leer "Indique el tamano del lado distinto de un triangulo isosceles", ladoDiferente
		if(ladoIgual<=0 o ladoDiferente<=0)
			Escribir "El lado de un triangulo debe tener un valor positivo mayor a 0"
		end-if
	while(ladoIgual<=0 o ladoDiferente<=0)
	perimetro <- (ladoIgual * 2) + ladoDiferente
	Escribir "El perimetro del triangulo es de ", perimetro
Fin