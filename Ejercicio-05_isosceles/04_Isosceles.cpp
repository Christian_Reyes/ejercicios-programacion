#include<iostream>
using namespace std;

main(){
	float ladoIgual, ladoDiferente, perimetro;
	do{
		cout<<"Indique el tamano de uno de los lados iguales de un triangulo isosceles:\n";
		cin>>ladoIgual;
		cout<<"Indique el tamano del lado distinto de un triangulo isosceles:\n";
		cin>>ladoDiferente;
		if(ladoIgual<=0 || ladoDiferente<=0)
			cout<<"El lado de un triangulo debe tener un valor positivo mayor a 0\n";
	}while(ladoIgual<=0 || ladoDiferente<=0);
	perimetro = (ladoIgual * 2) + ladoDiferente;
	cout<<"El perimetro del triangulo es de: "<<perimetro;
}
