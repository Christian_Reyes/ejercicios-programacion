#include<iostream>
using namespace std;

float equilatero(){
	float lado, perimetro;
	do{
		cout<<"Indique el lado de un triangulo equilatero:\n";
		cin>>lado;
		if(lado<=0)
			cout<<"El lado de un triangulo debe tener un valor positivo mayor a 0\n";
	}while(lado<=0);
	perimetro = lado * 3;
	return perimetro;
}

float isosceles(){
	float ladoIgual, ladoDiferente, perimetro;
	do{
		cout<<"Indique el tamano de uno de los lados iguales de un triangulo isosceles:\n";
		cin>>ladoIgual;
		cout<<"Indique el tamano del lado distinto de un triangulo isosceles:\n";
		cin>>ladoDiferente;
		if(ladoIgual<=0 || ladoDiferente<=0)
			cout<<"El lado de un triangulo debe tener un valor positivo mayor a 0\n";
	}while(ladoIgual<=0 || ladoDiferente<=0);
	perimetro = (ladoIgual * 2) + ladoDiferente;
	return perimetro;
}

float escaleno(){
	float ladoA, ladoB, ladoC, perimetro;
	
	do{
		cout<<"Indique la longitud del primer lado del triangulo:\n";
		cin>>ladoA;
		cout<<"Indique la longitud del segundo lado del triangulo:\n";
		cin>>ladoB;
		cout<<"Indique la longitud del tercer lado del triangulo:\n";
		cin>>ladoC;
		if(ladoA <= 0 || ladoB <= 0 || ladoC <= 0)
			cout<<"El lado de un triangulo debe tener un valor positivo mayor a 0\n";
	}while(ladoA <= 0 || ladoB <= 0 || ladoC <= 0);
	
	perimetro = ladoA + ladoB + ladoC;
	return perimetro;
}


main(){
	int opcion;
	float resultado;
	
	cout<<"Indique el tipo de triangulo para calcular su perimetro:\n\n";
	cout<<"1. Equilatero\n";
	cout<<"2. Isosceles\n";
	cout<<"3. Escaleno\n";
	cout<<"Opcion: ";
	cin>>opcion;
	
	if(opcion == 1)
		resultado = equilatero();
	else if(opcion == 2)
		resultado = isosceles();
	else if(opcion == 3)
		resultado = escaleno();
	else
		cout<<"No se selecciono una opci�n disponible. Fin del programa";
		
	cout<<"El perimetro del triangulo es de "<<resultado;
}
