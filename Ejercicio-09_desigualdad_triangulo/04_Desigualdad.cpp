#include<iostream>
using namespace std;

main(){
	int ladoA, ladoB, ladoC;
		
	cout<<"Se valorara que se pueda formar un triangulo con la medida de sus lados (Teorema de desigualdad triangular)";
	cout<<"\n\nIndique la medida del primer lado: "; 
	cin>>ladoA;
	cout<<"\nIndique la medida del segundo lado: "; 
	cin>>ladoB;
	cout<<"\nIndique la medida del tercer lado: "; 
	cin>>ladoC;
	if(ladoA < (ladoB+ladoC) &&  ladoB < (ladoA+ladoC) && ladoC < (ladoA+ladoB))
		cout<<"\nLos lados cumplen la propiedad de desigualdad. Se puede formar un triangulo";
	else
		cout<<"\nLos lados NO cumplen la propiedad de desigualdad. \nNo es posible formar un triangulo";
}
