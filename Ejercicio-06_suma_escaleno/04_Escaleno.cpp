#include<iostream>
using namespace std;

main(){
	float ladoA, ladoB, ladoC, perimetro;
	
	do{
		cout<<"Indique la longitud del primer lado del triangulo:\n";
		cin>>ladoA;
		cout<<"Indique la longitud del segundo lado del triangulo:\n";
		cin>>ladoB;
		cout<<"Indique la longitud del tercer lado del triangulo:\n";
		cin>>ladoC;
		if(ladoA <= 0 || ladoB <= 0 || ladoC <= 0)
			cout<<"El lado de un triangulo debe tener un valor positivo mayor a 0\n";
	}while(ladoA <= 0 || ladoB <= 0 || ladoC <= 0);
	
	perimetro = ladoA + ladoB + ladoC;
	cout<<"El perimetro del triangulo es de: "<<perimetro;
}
